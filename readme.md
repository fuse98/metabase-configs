# Metabase Configuration with PostgreSQL

## About

We use the dockerized version of metabase integrated with postgreSQL database also running on a docker container. 

This service provides tools to visualize and check data in various databases. It has its own database(we use postgreSQL for this.) to store configurations.

It is advisable to backup this internal database from time to time.


## Deploy

Deployment environment must have docker and docker-compose installed.

Provide two secret files required for running Postgres and Metabase:

  1- db_user.txt : should contain database username for postgres db
  
  2- db_password.txt : should contain password of postgres user

METABASE_PORT environment variable can be configured to customize the input port of metabase service. By default it uses 3020.

## Start

To start an already deployed container one should redirect to where docker-compose.yml file exists and run the following command:
```
docker-compose  up (-d)
```
The "-d" is for running metabase on background.

NOTE: Metabase can take a while to initialize.

## Backup

